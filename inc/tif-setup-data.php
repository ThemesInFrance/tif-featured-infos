<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Get Commitments setup data
 */

function tif_plugin_commitments_setup_data() {

	$boxed_width = class_exists ( 'Themes_In_France' ) ? tif_get_option( 'theme_init', 'tif_width,primary', 'lenght' ) : '1200';

	return $tif_commitments_setup_data = array(

		'tif_tabs'                  => 1,

		// Settings
		'tif_init'                  => array(
			'enabled'                   => true,
			'customizer_enabled'        => 1,
			'generated'                 => array( null ),
			'css_enabled'               => '',
			'capabilities'              => '',
			'custom_css'                => '',
		),

		// Layout
		'tif_commitments_layout' => array(
			'loop_attr'                 => array(
				'column',
				'3',
				'1.1',
				esc_html__( 'My featured title', 'tif-commitments' ),
				'h2',
				'col-span-full'
			),
			'boxed_width'               => $boxed_width,
			'features'                  => array(
				'boxed',
			),
			'posts_per_page'            => 6,
			'box'                       => array(
				'padding'               => 'spacer_medium_plus,spacer_medium_plus',
			),
			'alignment'                 => array(
				'gap'                   => 'gap_none,gap_none',
			),
			'button_text'               => esc_html__( 'Read now', 'tif-commitments' ),
			'thumb_rounded'             => 'radius_circle',
			'thumb_width'               => '200',
		),

		'tif_commitments_colors' => array(
			'bgcolor'                   => 'none,normal,1',
			'odd_bgcolor'               => '#ffffff,normal,1',
			'odd_thumbnail_bgcolor'     => '#f0f0f0,normal,1',
			'odd_text_color'            => '#000000',
			'even_bgcolor'              => '#f7f7f7,normal,1',
			'even_thumbnail_bgcolor'    => '#f0f0f0,normal,1',
			'even_text_color'           => '#000000',
		),

		// Other fields
		'tif_commitments_content'=> array(

			'subtitle'                  => array(
				'Featured Sub title',
				'h3',
				'col-span-full'
			),
			'description'               => array(
				'Nisi mihi Phaedrum, inquam, tu mentitum aut Zenonem putas, quorum utrumque audivi, cum mihi nihil sane praeter sedulitatem probarent, omnes mihi Epicuri sententiae satis notae sunt. atque eos, quos nominavi, cum Attico nostro frequenter audivi, cum miraretur ille quidem utrumque, Phaedrum autem etiam amaret, cotidieque inter nos ea, quae audiebamus, conferebamus, neque erat umquam controversia, quid ego intellegerem, sed quid probarem.',
				'div',
				'col-span-full'
			),

			'featured_1'                => array(
				// 'fa'                        => 'fa-mobile',
				'thumbnail'                 => TIF_COMMITMENTS_URL . '/assets/img/featured-01.png',
				'title'                     => array(
					esc_html__( 'Featured title #1', 'tif-commitments' ),
					'h3',
				),
				'subtitle'                  => array(
					esc_html__( 'Sub title #1', 'tif-commitments' ),
					'span',
				),
				'excerpt'                   => array(
					'Iamque non umbratis fallaciis res agebatur, sed qua palatium est extra muros, armatis omne circumdedit. ingressusque obscuro iam die, ablatis regiis indumentis Caesarem tunica texit et paludamento communi.',
					'p'
				),
				'link'                      => '#',
			),

			'featured_2'               => array(
				// 'fa'                        => 'fa-bell',
				'thumbnail'                 => TIF_COMMITMENTS_URL . '/assets/img/featured-02.png',
				'title'                     => array(
					esc_html__( 'Featured title #2', 'tif-commitments' ),
					'h3'
				),
				'subtitle'                  => array(
					esc_html__( 'Sub title #2', 'tif-commitments' ),
					'span',
				),
				'excerpt'                   => array(
					'Hac ita persuasione reducti intra moenia bellatores obseratis undique portarum aditibus, propugnaculis insistebant et pinnis, congesta undique saxa telaque habentes in promptu, ut si quis se proripuisset interius, multitudine missilium sterneretur et lapidum.',
					'p'
				),
				'link'                      => '#',
			),

			'featured_3'               => array(
				// 'fa'                        => 'fa-laptop',
				'thumbnail'                 => TIF_COMMITMENTS_URL . '/assets/img/featured-03.png',
				'title'                     => array(
					esc_html__( 'Featured title #2', 'tif-commitments' ),
					'h3'
				),
				'subtitle'                  => array(
					esc_html__( 'Sub title #2', 'tif-commitments' ),
					'span',
				),
				'excerpt'                   => array(
					'Sed laeditur hic coetuum magnificus splendor levitate paucorum incondita, ubi nati sunt non reputantium, sed tamquam indulta licentia vitiis ad errores lapsorum ac lasciviam. ut enim Simonides lyricus docet, beate perfecta ratione vieturo ante alia patriam esse convenit gloriosam.',
					'p'
				),
				'link'                      => '#',
			),

			'featured_4'               => array(
				// 'fa'                        => 'fa-mobile',
				'thumbnail'                 => TIF_COMMITMENTS_URL . '/assets/img/featured-04.png',
				'title'                     => array(
					esc_html__( 'Featured title #3', 'tif-commitments' ),
					'h3'
				),
				'subtitle'                  => array(
					esc_html__( 'Sub title #3', 'tif-commitments' ),
					'span',
				),
				'excerpt'                   => array(
					'Iamque non umbratis fallaciis res agebatur, sed qua palatium est extra muros, armatis omne circumdedit. ingressusque obscuro iam die, ablatis regiis indumentis Caesarem tunica texit et paludamento communi.',
					'p'
				),
				'link'                      => '#',
			),

			'featured_5'               => array(
				// 'fa'                        => 'fa-bell',
				'thumbnail'                 => TIF_COMMITMENTS_URL . '/assets/img/featured-05.png',
				'title'                     => array(
					esc_html__( 'Featured title #4', 'tif-commitments' ),
					'h3'
				),
				'subtitle'                  => array(
					esc_html__( 'Sub title #4', 'tif-commitments' ),
					'span',
				),
				'excerpt'                   => array(
					'Hac ita persuasione reducti intra moenia bellatores obseratis undique portarum aditibus, propugnaculis insistebant et pinnis, congesta undique saxa telaque habentes in promptu, ut si quis se proripuisset interius, multitudine missilium sterneretur et lapidum.',
					'p'
				),
				'link'                      => '#',
			),

			'featured_6'               => array(
				// 'fa'                        => 'fa-laptop',
				'thumbnail'                 => TIF_COMMITMENTS_URL . '/assets/img/featured-06.png',
				'title'                     => array(
					esc_html__( 'Featured title #5', 'tif-commitments' ),
					'h3'
				),
				'subtitle'                  => array(
					esc_html__( 'Sub title #5', 'tif-commitments' ),
					'span',
				),
				'excerpt'                   => array(
					'Donec tincidunt diam id urna fringilla, id aliquam nisl. Aenean ut tempor metus.',
					'p'
				),
				'link'                       => '#',
			),

		),

	);

}
