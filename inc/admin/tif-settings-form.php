<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_commitments_options_page() {

	$capability = tif_get_submenu_capability( tif_get_option( 'plugin_commitments', 'tif_init,capabilities', 'multicheck' ) ) ;

	if ( ! current_user_can( $capability ) )
		wp_die( esc_html__( 'Unauthorized user.', 'tif-commitments' ) );

	?>

	<div class="wrap">

		<h1 class="tif-plugin-title"><?php _e( 'Commitments', 'tif-commitments' ) ?></h1>
		<p><?php _e( 'Highlight important information on your homepage.', 'tif-commitments' ) ?></p>

		<form method="post" action="options.php" class="tif-form tif-plugin-metabox row tif-tabs">

			<?php

			settings_fields( 'tif-commitments' );
			// do_settings_sections( 'tif-commitments' );
			wp_nonce_field( 'tif_commitments_action', 'tif_commitments_nonce_field' );
			$tif_plugin_name = 'tif_plugin_commitments';

			// Create a new instance
			// Pass in a URL to set the action
			$form = new Tif_Form_Builder();

			// Form attributes are modified with the set_att function.
			// First argument is the setting
			// Second argument is the value
			$count = 0;
			$tabs = 0;

			$form->set_att( 'method', 'post' );
			$form->set_att( 'enctype', 'multipart/form-data' );
			$form->set_att( 'markup', 'html' );
			$form->set_att( 'class', 'tif-form tif-tabs' );
			$form->set_att( 'novalidate', false );
			$form->set_att( 'is_array', 'tif_plugin_commitments' );
			$form->set_att( 'form_element', false );
			$form->set_att( 'add_submit', true );

			/**
			 * New tab for administrator settings
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_commitments', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Settings', 'tif-commitments' ),
				'dashicon' => 'dashicons-admin-settings',
				'first' => true,
			) );

				require_once 'tif-settings-tab-settings.php';

			/**
			 * New tab for help
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_commitments', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Help', 'tif-commitments' ),
				'dashicon' => 'dashicons-editor-help',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

				require_once 'tif-settings-tab-help.php';

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</div>',
			) );

			// Create the form
			$form->build_form();

			?>

		</form>

	</div><!-- .wrap -->

	<?php

	// echo wp_kses( tif_plugin_powered_by(), tif_allowed_html() );
	echo wp_kses( tif_memory_usage(), tif_allowed_html() );

}
