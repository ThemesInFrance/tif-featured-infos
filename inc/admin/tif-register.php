<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Commitments Sanitize and register
 */

function tif_commitments_sanitize( $input ) {

	// Verify that the nonce is valid.
	if ( ! isset( $_POST['tif_commitments_nonce_field'] ) || ! wp_verify_nonce( $_POST['tif_commitments_nonce_field'], 'tif_commitments_action' ) ) :

		$code	 = 'nonce_unverified';
		$message = esc_html__( 'Sorry, your nonce did not verify.', 'tif-commitments' );
		$type	 = 'error';

		return $input;

	endif;

	$code	 = 'settings_updated';
	$message = esc_html__( 'Congratulations, the data has been successfully recorded.' , 'tif-commitments' );
	$type	 = 'updated';

	/**
	 * @link https://developer.wordpress.org/reference/functions/add_settings_error/
	 */
	add_settings_error(
		esc_attr( 'commitments_settings_error' ),
		esc_attr( $code ),
		esc_html__( $message ),
		$type
	);

	$capability = tif_get_submenu_capability( tif_get_option( 'plugin_commitments', 'tif_init,capabilities', 'multicheck' ) ) ;

	if ( ! current_user_can( $capability ) )
		wp_die( esc_html__( 'Unauthorized user.', 'tif-commitments' ) );

	$new_input =
		! empty( get_option( 'tif_plugin_commitments' ) )
		? get_option( 'tif_plugin_commitments' )
		: tif_plugin_commitments_setup_data() ;

	// Last Tab used
	$new_input['tif_tabs'] =
		! empty( $input['tif_tabs'] )
		? tif_sanitize_html( $input['tif_tabs'] )
		: false ;

	// Settings
	if(  current_user_can( 'manage_options' ) ) :
		$tif_init = array(
			'enabled'				=> 'checkbox',
			'customizer_enabled'	=> 'checkbox',
			'generated'				=> 'multicheck',
			'css_enabled'			=> 'key',
			'capabilities'			=> 'multicheck',
			'custom_css'			=> 'css',
		);

		foreach ( $tif_init as $key => $value) {
			$new_input['tif_init'][$key] =
				! empty( $input['tif_init'][$key] )
				? call_user_func( 'tif_sanitize_' . $value, $input['tif_init'][$key] )
				: false ;
		}
	endif;


	// Layout
	$tif_commitments_layout = array(
		'features'				=> 'multicheck',
		'boxed_width'		=> 'length',
		'layout'				=> 'loop_layout',
		'posts_per_page'		=> 'absint',
		'thumb_width'		=> 'absint',
		'thumb_rounded'		=> 'absint',
		'padding'				=> 'multicheck',
		'gap'					=> 'multicheck',
		'button_text'			=> 'string',
	);

	foreach ( $tif_commitments_layout as $key => $value) {
		$new_input['tif_commitments_layout'][$key] =
			! empty( $input['tif_commitments_layout'][$key] )
			? call_user_func( 'tif_sanitize_' . $value, $input['tif_commitments_layout'][$key] )
			: false ;
	}

	return $new_input;

}
