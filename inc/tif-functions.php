<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Commitments front render function
 */
function tif_render_commitments() {

	if ( is_paged() )
		return;

	if( null == tif_get_option( 'plugin_commitments', 'tif_init,enabled', 'checkbox' ) ) {

		if ( current_user_can( 'customize' ) ) {

		?>

		<section id="tif-commitments" class="tif-commitments container">

			<div class="inner tif-boxed">

				<div class="tif-alert tif-alert--warning">

					<?php

					if ( is_customize_preview() ) :
						echo '<span id="tif-commitments-disabled"></span>';
						printf(
							'<p>%s</p>',
							esc_html__( 'Commitments is disabled.', 'tif-commitments' )
						);
					endif;

					if ( ! is_customize_preview() )
					printf(
						'<p><a href="%2$s">%1$s</a>.</p>',
						esc_html__( 'Commitments can be enabled in the customizer', 'tif-commitments' ),
						admin_url( 'customize.php?autofocus[section]=tif_plugin_commitments_panel_settings_section' )
					);

					?>

				</div>

			</div><!-- .inner -->

		</section><!-- #tif-callout -->

		<?php

		}

		return;

	}

	$layout_settings = tif_get_option( 'plugin_commitments', 'tif_commitments_layout', 'array' );
	$loop_attr       = tif_get_loop_attr( $layout_settings['loop_attr'] );

	$features        = tif_sanitize_multicheck( $layout_settings['features'] );
	$boxed           = in_array( "boxed", (array)$features ) ? ' tif-boxed' : false ;

	$width           = absint( $layout_settings['thumb_width'] );
	$height          = tif_get_height_from_ratio( $width, $loop_attr['thumbnail']['ratio'] );

	$dir             = Tif_Commitments_Init::tif_plugin_assets_dir();
	$path            = $dir['basedir'] . '/assets/img/blank-' . $width . 'x' . $height . '.png';
	$url             = is_customize_preview()
		? TIF_COMMITMENTS_URL . '/tif/assets/img/blank.php?w=' . $width . '&h=' . $height
		: $dir['baseurl'] . '/assets/img/blank-' . $width . 'x' . $height . '.png';
	tif_build_blank_thumbnail( $width, $height, $path );

	$colors          = tif_get_option( 'plugin_commitments', 'tif_commitments_colors', 'array' );
	$custom_colors   = new Tif_Custom_Colors;
	$odd_bgcolor     = $custom_colors->tif_get_color_from_key( $colors['odd_thumbnail_bgcolor'] );
	$even_bgcolor    = $custom_colors->tif_get_color_from_key( $colors['even_thumbnail_bgcolor'] );

	$feat_attr       = array(
		'wrap_container'    => array(
			'wrap'              => 'section',
			'additional'        => array(
				'id'                => 'tif-commitments',
				'class'             => 'tif-commitments container' ),
		),
		'container'         => array(
			'additional'    => array(
				'class'             => 'tif-featured-area ' . esc_attr( $boxed )
			),
		),
		'post'              => array(
			'additional'        => array(
				'class'             => 'featured '
			),
		'inner'             => array(
			'wrap'              => false,
			'attr'              => array(),
			'additional'        => array()
		),
		),
	);

	$loop_attr = tif_parse_args_recursive( $feat_attr, $loop_attr );

	$wrap_container = $loop_attr['wrap_container']['wrap'] ? '<' . $loop_attr['wrap_container']['wrap'] . tif_parse_attr( $loop_attr['wrap_container']['attr'], $loop_attr['wrap_container']['additional'] ) . '>' : false ;
	$container      = $loop_attr['container']['wrap'] ? '<' . $loop_attr['container']['wrap'] . tif_parse_attr( $loop_attr['container']['attr'], $loop_attr['container']['additional'] ) . '>' : false ;
	$inner          = $loop_attr['inner']['wrap'] ? '<' . $loop_attr['inner']['wrap'] . tif_parse_attr( $loop_attr['inner']['attr'], $loop_attr['inner']['additional'] ) . '>' : false ;
	$post_inner     = isset( $loop_attr['post']['inner']['wrap'] ) && $loop_attr['post']['inner']['wrap'] ? '<' . $loop_attr['post']['inner']['wrap'] . tif_parse_attr( $loop_attr['post']['inner']['attr'], $loop_attr['post']['inner']['additional'] ) . '>' : false ;
	$wrap_content   = isset( $loop_attr['post']['wrap_content']['wrap'] ) && $loop_attr['post']['wrap_content']['wrap'] ? '<' . $loop_attr['post']['wrap_content']['wrap'] . tif_parse_attr( $loop_attr['post']['wrap_content']['attr'] ) . '>' : false ;

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

		// Open Container if there is one
		if ( $container ) echo $container . "\n\n" ;

			// Open Inner if there is one
			if ( $inner ) echo $inner . "\n\n" ;

			$feat_content = tif_get_option( 'plugin_commitments', 'tif_commitments_content', 'array' );

			if ( null != $loop_attr['main_title']['title'] ) {

				echo '<' . esc_attr( $loop_attr['main_title']['wrap'] ) . ' ' .
					tif_parse_attr( array( 'class' => 'tif-featured-title '), $loop_attr['main_title']['additional'] ) .
					'><span>';
				echo wp_kses( $loop_attr['main_title']['title'], tif_allowed_html() );
				echo '</span></' . esc_attr( $loop_attr['main_title']['wrap'] ) . '>';

			}

			if( ! empty( $feat_content['subtitle'] ) ){

				$feat_subtitle = tif_sanitize_wrap_attr( $feat_content['subtitle'] );

				if( isset( $feat_subtitle[0] ) && $feat_subtitle[0] ){

					echo '<' . esc_attr( $feat_subtitle[1] ) . ' class="tif-featured-sub-title ' . esc_attr( $feat_subtitle[2] ) . '">';
					echo wp_kses( $feat_subtitle[0], tif_allowed_html() );
					echo '</' . esc_attr( $feat_subtitle[1] ) . '>';

				}

			}

			if( ! empty( $feat_content['description'] ) ){

				$feat_description = tif_sanitize_wrap_attr( $feat_content['description'] );

				if( isset( $feat_description[0] ) && $feat_description[0] ){

					echo '<' . esc_attr( $feat_description[1] ) . ' class="featured-description ' . esc_attr( $feat_description[2] ) . '">';
					echo wp_kses( $feat_description[0], tif_allowed_html() );
					echo '</' . esc_attr( $feat_description[1] ) . '>';

				}

			}

			for ( $i = 1; $i <= (int)$layout_settings['posts_per_page']; $i++ ) {

				if( $i % 2 == 0 )
					$bgcolor = $even_bgcolor; // Even;

				else
					$bgcolor = $odd_bgcolor; // Odd

				$feat[$i]          = $feat_content['featured_' . $i];
				$feat_link[$i]     = ! empty( $feat[$i]['link'] ) ? $feat[$i]['link'] : null;
				// $feat_fa[$i]       = ! empty( $feat[$i]['fa'] ) ? $feat[$i]['fa'] : null;
				$feat_thumb[$i]    = ! empty( $feat[$i]['thumbnail'] ) ? $feat[$i]['thumbnail'] : null;
				$feat_title[$i]    = ! empty( $feat[$i]['title'] ) ? tif_sanitize_wrap_attr( $feat[$i]['title'] ) : null;
				$feat_subtitle[$i] = ! empty( $feat[$i]['subtitle'] ) ? tif_sanitize_wrap_attr( $feat[$i]['subtitle'] ) : null;
				$feat_excerpt[$i]  = ! empty( $feat[$i]['excerpt'] ) ? tif_sanitize_wrap_attr( $feat[$i]['excerpt'] ) : null;
				$link_open[$i]     = null != $feat_link[$i] ? '<a href="' . esc_url_raw( $feat_link[$i] ) . '">' : '';
				$link_close[$i]    = null != $link_open[$i] ? '</a>' : null ;

				$bgimg[$i]         = ! empty( $feat_thumb[$i] )
					? 'style="
					background:0 0/cover;
					background-image:url(' . esc_url_raw( $feat_thumb[$i] ) . ');
					background-size:cover;
					background-repeat:no-repeat;
					background-position:center;
					background-color:' . $bgcolor . '"' : false ;

				$tumbnail		 = str_replace ( ' href="', ' rel="nofollow" href="', $link_open[$i] ) . '<div class="entry-thumbnail ' . tif_esc_css( $loop_attr['thumbnail']['attr']['class'] ) . '">';
				$tumbnail		.= '<div ' . $bgimg[$i] . '>';
				$tumbnail		.= '<img src="' . esc_url_raw( $url ) . '" alt="' . esc_html( $feat_title[$i][0] ) . '" title="' . esc_html( $feat_title[$i][0] ) . '" />';

				// if ( ! $feat_thumb[$i] )
				// 	$tumbnail	.= '<div class="hover"><span><i class="fa ' . esc_attr( $feat_fa[$i] ) . '"></i></span></div>';

				$tumbnail		.= '</div>';
				$tumbnail		.= '</div>' . $link_close[$i];

				echo '<article class="' . tif_esc_css( $loop_attr['post']['attr']['class'] . ' ' . $loop_attr['post']['additional']['class'] ) . ' count-' . (int)$i . '">';

				// Open Post Inner if there is one
				if ( $post_inner ) echo $post_inner . "\n\n" ;

					echo $tumbnail;

					// Open Wrap Content if there is one
					if ( $wrap_content ) echo $wrap_content . "\n\n" ;

						$output = '';

						if( isset( $feat_title[$i][0] ) && $feat_title[$i][0] ){

							$output  = '';
							$output .= '<' . esc_attr( $feat_title[$i][1] ) . ' class="entry-title ' .
								esc_attr( isset( $feat_title[$i][2] ) && $feat_title[$i][2] ? $feat_title[$i][2] : null ) .
								'"><span>';
							$output .= $link_open[$i];
							$output .= wp_kses( $feat_title[$i][0], tif_allowed_html() );
							$output .= $link_close[$i];
							$output .= '</span></' . esc_attr( $feat_title[$i][1] ) . '>';

						}

						if( isset( $feat_subtitle[$i][0] ) && $feat_subtitle[$i][0] ){

							$output .='<' . esc_attr( $feat_subtitle[$i][1] ) . ' class="entry-sub-title' .
								esc_attr( isset( $feat_subtitle[$i][2] ) && $feat_subtitle[$i][2] ? $feat_subtitle[$i][2] : null ) .
								'"><span>';
							$output .= wp_kses( $feat_subtitle[$i][0], tif_allowed_html() );
							$output .='</span></' . esc_attr( $feat_subtitle[$i][1] ) . '>';

						}

						if( isset( $feat_excerpt[$i][0] ) && $feat_excerpt[$i][0] ){

							$output .='<' . esc_attr( $feat_excerpt[$i][1] ) . ' class="entry-content' .
								esc_attr( isset( $feat_excerpt[$i][2] ) && $feat_excerpt[$i][2] ? $feat_excerpt[$i][2] : null ) .
								'">';
							$output .= apply_filters( 'the_content', $feat_excerpt[$i][0] );
							$output .='</' . esc_attr( $feat_excerpt[$i][1] ) . '>';

						}

						echo $output;

						if ( null != $layout_settings['button_text'] )
							echo '<p class="button-area"><a href="' . esc_url_raw( $feat_link[$i] ) . '" class="btn btn--default customize-unpreviewable">' . esc_html( $layout_settings['button_text'] ) . '</a></p>';

					// Close Post Inner if there is one
					if ( $wrap_content ) echo '</' . $loop_attr['post']['wrap_content']['wrap'] . '>' . "\n\n" ;

				// Close Post Inner if there is one
				if ( $post_inner ) echo '</' . $loop_attr['post']['inner']['wrap'] . '>' . "\n\n" ;

				echo '</article>';

			}

			// Close Inner if there is one
			if ( $inner ) echo '</' . $loop_attr['inner']['wrap'] . '>' . "\n\n" ;

		// Close Container if there is one
		if ( $container ) echo '</' . $loop_attr['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $loop_attr['wrap_container']['wrap'] . '>' . "\n\n" ;

}
