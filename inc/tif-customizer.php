<?php

if ( ! defined( 'ABSPATH' ) ) exit;

if( ! tif_get_option( 'plugin_commitments', 'tif_init,customizer_enabled', 'checkbox' ) )
	return;

/**
 * Contains methods for customizing the theme customization screen.
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 */

class Tif_Commitments_Customize {

	/**
	 * This hooks into 'customize_register' ( available as of WP 3.4 ) and allows
	 * you to add new sections and controls to the Theme Customize screen.
	 *
	 * Note: To enable instant preview, we have to actually write a bit of custom
	 * javascript. See live_preview() for more.
	 *
	 * @see add_action( 'customize_register',$func )
	 * @param \WP_Customize_Manager $wp_customize
	 * @link http://ottopress.com/2012/how-to-leverage-the-theme-customizer-in-your-own-themes/
	 */

	public static function register ( $wp_customize ) {

		/**
		 * Assigning plugin name
		 */
		$tif_plugin_name = 'tif_plugin_commitments';

		// === PANEL // Commitments PANEL ===================================

		$wp_customize->add_panel(
			'tif_plugin_commitments_panel',
			array(
				'capabitity'		=> 'edit_theme_options',
				'priority'			=> 1030,
				'title'				=> esc_html__( 'Commitments', 'tif-commitments' )
			)
		);

		// ... SECTION // Commitments settings ..............................

		$wp_customize->add_section(
			'tif_plugin_commitments_panel_settings_section',
			array(
				'title'					=> esc_html__( 'Infos settings', 'tif-commitments' ),
				'panel'					=> 'tif_plugin_commitments_panel'
			)
		);

		// Hide Index Commitments Section
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_init][enabled]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_init,enabled' ),
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'sanitize_text_field',
				'type'				=> 'option'
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_init][enabled]',
			array(
				'label'				=> esc_html__( 'Commitments Enabled', 'tif-commitments' ),
				'section'			=> 'tif_plugin_commitments_panel_settings_section',
				'type'				=> 'checkbox',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			$tif_plugin_name . '[tif_init][enabled]',
			array(
				'selector' => '#tif-commitments-disabled',
			)
		);

		// ... SECTION // Commitments Layout ................................

		$wp_customize->add_section(
			'tif_plugin_commitments_panel_layout_section',
			array(
				'title'					=> esc_html__( 'Infos layout', 'tif-commitments' ),
				'panel'					=> 'tif_plugin_commitments_panel'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_layout][features]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_layout,features', 'multicheck' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Checkbox_Multiple_Control(
				$wp_customize, $tif_plugin_name . '[tif_commitments_layout][features]',
				array(
					'section' => 'tif_plugin_commitments_panel_layout_section',
					'label'   => esc_html__( 'Features', 'tif-commitments' ),
					'choices' => array(
						'boxed'			=> esc_html__( 'Commitments are boxed', 'tif-commitments' ),
						// 'rounded'		=> esc_html__( 'Header have rounded background', 'tif-commitments' ),
					)
				)
			)
		);
		$wp_customize->selective_refresh->add_partial(
			$tif_plugin_name . '[tif_commitments_layout][features]',
			array(
				'selector' => '#infos .tif-featured-area',
			)
		);

		if ( ! class_exists( 'Themes_In_France' ) ) {

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_commitments_layout][boxed_width]',
				array(
					'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_layout,boxed_width', 'length' ),
					'type'				=> 'option',
					'capability'		=> 'edit_theme_options',
					'sanitize_callback'	=> 'tif_sanitize_length'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Number_Multiple_Control(
					$wp_customize,
					$tif_plugin_name . '[tif_commitments_layout][boxed_width]',
					array(
						'section'			=> 'tif_plugin_commitments_panel_layout_section',
						'label'				=> esc_html__( 'Boxed width', 'tif-commitments' ),
						'choices'			=> array(
							'value'				=> esc_html__( 'Value', 'tif-commitments' ),
							'unit'				=> esc_html__( 'Unit', 'tif-commitments' ),
						),
						'input_attrs'		=> array(
							'min'				=> '800',
							// 'max'				=> '50',
							'step'				=> '1',
							'unit'				=> array(
								'px'				=> esc_html__( 'px', 'tif-commitments' ),
							),
							'alignment'			=> 'column',
						),
					)
				)
			);

		}

		$wp_customize->add_setting(
			'tif_plugin_commitments_panel_layout_section_loop_layout_heading',
			array(
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_commitments_panel_layout_section_loop_layout_heading',
				array(
					'section'		=> 'tif_plugin_commitments_panel_layout_section',
					'label'			=> esc_html__( 'Loop layout', 'tif-commitments' ),
				)
			)
		);

		// Add Setting
		// ...
		$tif_commitments_layout      = tif_get_default( 'plugin_commitments', 'tif_commitments_layout,loop_attr', 'loop_attr' );
		$tif_commitments_layout_attr = tif_sanitize_loop_attr( $tif_commitments_layout );
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_layout][loop_attr]',
			array(
				'default'			=> (array)$tif_commitments_layout,
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_loop_attr'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Wrap_Attr_Main_Layout_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_layout][loop_attr]',
				array(
					'section'			=> 'tif_plugin_commitments_panel_layout_section',
					'label'				=> esc_html__( 'Layout', 'tif-commitments' ),
					'choices'           => array(
						0	=> array(
							'id'         => 'layout',
							'label'         => false,
							// 'description' => sprintf( '%s "%s"',
							// 	esc_html__( 'Default value:', 'tif-commitments' ),
							// 	esc_html__( 'Medium', 'tif-commitments' ),
							// )
							'choices'			=> array(
								'column'            => array( TIF_COMMITMENTS_ADMIN_IMAGES_URL . '/layout-loop-column.png', esc_html_x( 'Column', 'Loop layout', 'tif-commitments' ) ),
								'media_text_1_3'    => array( TIF_COMMITMENTS_ADMIN_IMAGES_URL . '/layout-loop-media-text-1-3.png', esc_html_x( 'Media/Text (1/3)', 'Loop layout', 'tif-commitments' ) ),
								'media_text'        => array( TIF_COMMITMENTS_ADMIN_IMAGES_URL . '/layout-loop-media-text.png', esc_html_x( 'Media/Text', 'Loop layout', 'tif-commitments' ) ),
								'none'              => array( TIF_COMMITMENTS_ADMIN_IMAGES_URL . '/layout-none.png', esc_html_x( 'None', 'Loop layout', 'tif-commitments' ) )
							),
						),
						1	=> array(
							'id'          => 'post_per_line',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-commitments' ),
								(int)$tif_commitments_layout_attr[1],
							)
						),
						2	=> array(
							'id'          => 'thumbnail_ratio',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-commitments' ),
								esc_html__( 'Medium', 'tif-commitments' ),
							)
						),
						3	=> array(
							'id'          => 'title',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-commitments' ),
								esc_html( $tif_commitments_layout_attr[3] ),
							)
						),
						4	=> array(
							'id'          => 'title_tag',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-commitments' ),
								esc_html( $tif_commitments_layout_attr[4] ),
							)
						),
						5	=> array(
							'id'          => 'title_class',
							'description' => sprintf( '%s %s "%s"',
								esc_html__( 'You can use the "screen-reader-text" class to hide the title. It will remain accessible to screen readers.', 'tif-commitments' ),
								esc_html__( 'Default value:', 'tif-commitments' ),
								esc_html( $tif_commitments_layout_attr[5] ),
							)
						),
						6	=> array(
							'id'          => 'container_class',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-commitments' ),
								esc_html( $tif_commitments_layout_attr[6] ),
							)
						),
						7	=> array(
							'id'          => 'post_class',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-commitments' ),
								esc_html( $tif_commitments_layout_attr[7] ),
							)
						),
					),
					'settings'		=> $tif_plugin_name . '[tif_commitments_layout][loop_attr]'
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_layout][features]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_layout,features', 'multicheck' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Checkbox_Multiple_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_layout][features]',
				array(
					'section' => 'tif_plugin_commitments_panel_layout_section',
					'label'   => esc_html__( 'Features', 'tif-commitments' ),
					'choices' => array(
						'boxed'				=> esc_html__( 'Boxed', 'tif-commitments' ),
					)
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_layout][posts_per_page]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_layout,posts_per_page', 'absint' ),
				'type'				=> 'option',
				'sanitize_callback'	=> 'absint',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_commitments_layout][posts_per_page]',
			array(
				'section'			=> 'tif_plugin_commitments_panel_layout_section',
				'type'				=> 'number',
				'label'				=> esc_html__( 'How many posts to display?', 'tif-commitments' ),
				'input_attrs'		=> array(
					'min' => '0',
					'max' => '6',
					'step' => '1',
				),
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_layout][button_text]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_layout,button_text', 'string' ),
				'sanitize_callback'	=> 'sanitize_text_field',
				'capability'		=> 'edit_theme_options',
				'type'				=> 'option',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_commitments_layout][button_text]',
			array(
				'label'				=> esc_html__( 'Button text', 'tif-commitments' ),
				'description'		=> esc_html__( 'Leave blank to disable.', 'tif-commitments' ),
				'section'			=> 'tif_plugin_commitments_panel_layout_section',
				'type'				=> 'text',
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_commitments_panel_layout_section_subtitle_heading',
			array(
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_commitments_panel_layout_section_subtitle_heading',
				array(
					'section'		=> 'tif_plugin_commitments_panel_layout_section',
					'label'			=> esc_html__( 'Sub title', 'tif-commitments' ),
				)
			)
		);

		$tif_commitments_subtitle = tif_get_default( 'plugin_commitments', 'tif_commitments_content,subtitle', 'multicheck' );
		$tif_commitments_subtitle = tif_sanitize_wrap_attr( $tif_commitments_subtitle );
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_content][subtitle]',
			array(
				'default'			=> (array)$tif_commitments_subtitle,
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Wrap_Attr_Content_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_content][subtitle]',
				array(
					'section'			=> 'tif_plugin_commitments_panel_layout_section',
					// 'label'				=> 'esc_html__'( 'Sub title', 'tif-commitments' ),
					'choices'           => array(
						0	=> array(
							'id'          => 'content',
							'type'        => 'text',
							// 'description' => sprintf( '%s "%s"',
							// 	esc_html__( 'Default value:', 'tif-commitments' ),
							// 	esc_html( isset( $tif_commitments_subtitle[0] ) ? $tif_commitments_subtitle[0] : null ),
							// )
						),
						1	=> array(
							'id'          => 'container_tag',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-commitments' ),
								esc_html( isset( $tif_commitments_subtitle[1] ) ? $tif_commitments_subtitle[1] : null ),
							)
						),
						2	=> array(
							'id'          => 'container_class',
							// 'description' => sprintf( '%s "%s"',
							// 	esc_html__( 'Default value:', 'tif-commitments' ),
							// 	esc_html( isset( $tif_commitments_subtitle[2] ) ? $tif_commitments_subtitle[2] : null ),
							// )
						),
					),
					'settings'			=> $tif_plugin_name . '[tif_commitments_content][subtitle]'
				)
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_commitments_panel_layout_section_description_heading',
			array(
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_commitments_panel_layout_section_description_heading',
				array(
					'section'		=> 'tif_plugin_commitments_panel_layout_section',
					'label'			=> esc_html__( 'Description', 'tif-commitments' ),
				)
			)
		);

		$tif_commitments_description = tif_get_default( 'plugin_commitments', 'tif_commitments_content,description', 'wrap_attr' );
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_content][description]',
			array(
				'default'			=> (array)$tif_commitments_description,
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Wrap_Attr_Content_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_content][description]',
				array(
					'section'			=> 'tif_plugin_commitments_panel_layout_section',
					// 'label'				=> esc_html__( 'Description', 'tif-commitments' ),
					'choices'           => array(
						0	=> array(
							'id'          => 'content',
							// 'type'        => 'text',
							// 'description' => sprintf( '%s "%s"',
							// 	esc_html__( 'Default value:', 'tif-commitments' ),
							// 	esc_html( isset( $tif_commitments_description[0] ) ? $tif_commitments_description[0] : null ),
							// )
						),
						1	=> array(
							'id'          => 'container_tag',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-commitments' ),
								esc_html( isset( $tif_commitments_description[1] ) ? $tif_commitments_description[1] : null ),
							)
						),
						2	=> array(
							'id'          => 'container_class',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-commitments' ),
								esc_html( isset( $tif_commitments_description[2] ) ? $tif_commitments_description[2] : null ),
							)
						),
					),
					'settings'			=> $tif_plugin_name . '[tif_commitments_content][description]'
				)
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_commitments_panel_layout_section_header_heading',
			array(
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_commitments_panel_layout_section_header_heading',
				array(
					'section'			=> 'tif_plugin_commitments_panel_layout_section',
					'label'				=> esc_html__( 'Thumbnail', 'tif-commitments' ),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_layout][thumb_width]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_layout,thumb_width', 'absint' ),
				'type'				=> 'option',
				'sanitize_callback'	=> 'absint',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_commitments_layout][thumb_width]',
			array(
				'section'			=> 'tif_plugin_commitments_panel_layout_section',
				'type'				=> 'number',
				'label'				=> esc_html__( 'Width in pixels', 'tif-commitments' ),
				'input_attrs'		=> array(
					'min' => '0',
					'max' => '500',
					'step' => '1',
				),
			)
		);

		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_layout][thumb_rounded]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_layout,thumb_rounded', 'scss_variables' ),
				'type'				=> 'select',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_scss_variables'
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_commitments_layout][thumb_rounded]',
			array(
				'section'			=> 'tif_plugin_commitments_panel_layout_section',
				'label'				=> esc_html__( 'Rounded?', 'tif-commitments' ),
				'type'				=> 'select',
				'choices'			=> tif_get_border_radius_array( 'label' ),
				'input_attrs'		=> array(
					'alignment'         => 'colum',
				),
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_commitments_panel_layout_section_spacing_heading',
			array(
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_commitments_panel_layout_section_spacing_heading',
				array(
					'section'			=> 'tif_plugin_commitments_panel_layout_section',
					'label'				=> esc_html__( 'Spacing', 'tif-commitments' ),
				)
			)
		);

		$tif_commitments_layout_box = tif_get_default( 'plugin_commitments', 'tif_commitments_layout,box', 'array' );

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_layout][box][padding]',
			array(
				'default'			=> tif_sanitize_scss_variables( $tif_commitments_layout_box['padding'] ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_scss_variables'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Select_Multiple_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_layout][box][padding]',
				array(
					'section'			=> 'tif_plugin_commitments_panel_layout_section',
					'label'				=> esc_html__( 'Padding', 'tif-commitments' ),
					'label'				=> esc_html__( 'Padding', 'tif-commitments' ),
					'choices'			=> array(
						0					=> array(
							'label'				=> esc_html__( 'Vertical', 'tif-commitments' ),
							'choices'			=> tif_get_spacers_array( 'label' )
						),
						1					=> array(
							'label'				=> esc_html__( 'Horizontal', 'tif-commitments' ),
							'choices'			=> tif_get_spacers_array( 'label' )
						)
					),
					'input_attrs'		=> array(
						'alignment'			=> 'row'
					),
				)
			)
		);

		$tif_commitments_layout_alignment = tif_get_default( 'plugin_commitments', 'tif_commitments_layout,alignment', 'array' );

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_layout][alignment][gap]',
			array(
				'default'			=> tif_sanitize_scss_variables( $tif_commitments_layout_alignment['gap'] ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_scss_variables'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Select_Multiple_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_layout][alignment][gap]',
				array(
					'section'			=> 'tif_plugin_commitments_panel_layout_section',
					'label'				=> esc_html__( 'Spacing', 'tif-commitments' ),
					'choices'			=> array(
						0					=> array(
							'label'				=> esc_html__( 'Vertical', 'tif-commitments' ),
							'choices'			=> tif_get_gap_array( 'label' )
						),
						1					=> array(
							'label'				=> esc_html__( 'Horizontal', 'tif-commitments' ),
							'choices'			=> tif_get_gap_array( 'label' )
						)
					),
					'input_attrs'		=> array(
						'alignment'			=> 'row'
					),
				)
			)
		);

		// ... SECTION // Commitments Colors ................................

		$wp_customize->add_section(
			'tif_plugin_commitments_panel_colors_section',
			array(
				'title'				=> esc_html__( 'Infos colors', 'tif-commitments' ),
				'panel'				=> 'tif_plugin_commitments_panel'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_colors][bgcolor]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_colors,bgcolor', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_colors][bgcolor]',
				array(
					'section'			=> 'tif_plugin_commitments_panel_colors_section',
					'label'				=> esc_html__( 'Background color', 'tif-commitments' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> false,
					),
				)
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_commitments_panel_colors_section_odd_heading',
			array(
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_commitments_panel_colors_section_odd_heading',
				array(
					'section'			=> 'tif_plugin_commitments_panel_colors_section',
					'label'				=> esc_html__( 'Odd featured info', 'tif-commitments' ),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_colors][odd_bgcolor]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_colors,odd_bgcolor', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_colors][odd_bgcolor]',
				array(
					'section'			=> 'tif_plugin_commitments_panel_colors_section',
					'label'				=> esc_html__( 'Background color', 'tif-commitments' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> true,
					),
				)
			)
		);

		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_colors][odd_text_color]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_colors,odd_text_color', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_colors][odd_text_color]',
				array(
					'section'		=> 'tif_plugin_commitments_panel_colors_section',
					'label'			=> esc_html__( 'Text color', 'tif-commitments' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> false,
					),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_colors][odd_thumbnail_bgcolor]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_colors,odd_thumbnail_bgcolor', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_colors][odd_thumbnail_bgcolor]',
				array(
					'section'			=> 'tif_plugin_commitments_panel_colors_section',
					'label'				=> esc_html__( 'Thumbnail background color', 'tif-commitments' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> true,
					),
				)
			)
		);


		$wp_customize->add_setting(
			'tif_plugin_commitments_panel_colors_section_even_heading',
			array(
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_commitments_panel_colors_section_even_heading',
				array(
					'section'			=> 'tif_plugin_commitments_panel_colors_section',
					'label'				=> esc_html__( 'Even featured info', 'tif-commitments' ),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_colors][even_bgcolor]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_colors,even_bgcolor', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_colors][even_bgcolor]',
				array(
					'section'			=> 'tif_plugin_commitments_panel_colors_section',
					'label'				=> esc_html__( 'Background color', 'tif-commitments' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> true,
					),
				)
			)
		);

		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_colors][even_text_color]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_colors,even_text_color', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_colors][even_text_color]',
				array(
					'section'		=> 'tif_plugin_commitments_panel_colors_section',
					'label'			=> esc_html__( 'Text color', 'tif-commitments' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> false
					),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_commitments_colors][even_thumbnail_bgcolor]',
			array(
				'default'			=> tif_get_default( 'plugin_commitments', 'tif_commitments_colors,even_thumbnail_bgcolor', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_commitments_colors][even_thumbnail_bgcolor]',
				array(
					'section'			=> 'tif_plugin_commitments_panel_colors_section',
					'label'				=> esc_html__( 'Thumbnail background color', 'tif-commitments' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> true,
					),
				)
			)
		);

		// ... SECTION // Commitments Blocs #1 to #6 ........................

		for ( $i = 1; $i <= 6; $i++ ) {

			$featured[$i] = tif_get_default( 'plugin_commitments', 'tif_commitments_content,featured_' . $i, 'array' );

			$wp_customize->add_section(
				'tif_plugin_commitments_panel_featured' . (int)$i . '_section' ,
				array(
					/* translators: Bloc number */
					'title'					=> sprintf( esc_html_x( 'Info #%1$s', 'Bloc', 'tif-commitments' ), $i ),
					'panel'					=> 'tif_plugin_commitments_panel',
					'sanitize_callback'		=> 'sanitize_text_field',
				)
			);

			$wp_customize->add_setting(
				'tif_plugin_commitments_panel_featured' . (int)$i . '_section_header_heading',
				array(
					'sanitize_callback'	=> 'tif_sanitize_key'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Heading_Control(
					$wp_customize,
					'tif_plugin_commitments_panel_featured' . (int)$i . '_section_header_heading',
					array(
						'section'		=> 'tif_plugin_commitments_panel_featured' . (int)$i . '_section',
						'label'			=> esc_html__( 'Header', 'tif-commitments' ),
					)
				)
			);

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][thumbnail]',
				array(
					'default'			=> esc_url_raw( $featured[$i]['thumbnail'] ),
					'sanitize_callback'	=> 'esc_url_raw',
					'type'				=> 'option',
				)
			);

			$wp_customize->add_control(
				new WP_Customize_Image_Control(
					$wp_customize, $tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][thumbnail]',
					array(
						'label'			=> esc_html__( 'Image', 'tif-commitments' ),
						'section'		=> 'tif_plugin_commitments_panel_featured' . (int)$i . '_section',
						'settings'		=> $tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][thumbnail]',
					)
				)
			);

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][link]',
				array(
					'default'			=> esc_url_raw( $featured[$i]['link'] ),
					'capability'		=> 'edit_theme_options',
					'sanitize_callback'	=> 'sanitize_url',
					'type'				=> 'option'
				)
			);
			$wp_customize->add_control(
				$tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][link]',
				array(
					'label'				=> esc_html__( 'Link', 'tif-commitments' ),
					'section'			=> 'tif_plugin_commitments_panel_featured' . (int)$i . '_section',
					'type'				=> 'url',
				)
			);

			$wp_customize->add_setting(
				'tif_plugin_commitments_panel_featured' . (int)$i . '_section_title_heading',
				array(
					'sanitize_callback'	=> 'tif_sanitize_key'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Heading_Control(
					$wp_customize,
					'tif_plugin_commitments_panel_featured' . (int)$i . '_section_title_heading',
					array(
						'section'		=> 'tif_plugin_commitments_panel_featured' . (int)$i . '_section',
						'label'			=> esc_html__( 'Title', 'tif-commitments' ),
					)
				)
			);

			$tif_commitments_title[$i] = tif_sanitize_wrap_attr( $featured[$i]['title'] );
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][title]',
				array(
					'default'			=> (array)$tif_commitments_title[$i],
					'type'				=> 'option',
					'capability'		=> 'edit_theme_options',
					'sanitize_callback'	=> 'tif_sanitize_multicheck'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Wrap_Attr_Content_Control(
					$wp_customize,
					$tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][title]',
					array(
						'section'			=> 'tif_plugin_commitments_panel_featured' . (int)$i . '_section',
						// 'label'				=> esc_html__( 'Title', 'tif-commitments' ),
						'choices'           => array(
							0	=> array(
								'id'          => 'content',
								'type'        => 'text',
								// 'description' => sprintf( '%s "%s"',
								// 	esc_html__( 'Default value:', 'tif-commitments' ),
								// 	esc_html( isset( $tif_commitments_title[$i][0] ) ? $tif_commitments_title[$i][0] : null ),
								// )
							),
							1	=> array(
								'id'          => 'container_tag',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-commitments' ),
									esc_html( isset( $tif_commitments_title[$i][1] ) ? $tif_commitments_title[$i][1] : null ),
								)
							),
							2	=> array(
								'id'          => 'container_class',
								// 'description' => sprintf( '%s "%s"',
								// 	esc_html__( 'Default value:', 'tif-commitments' ),
								// 	esc_html( isset( $tif_commitments_title[$i][2] ) ? $tif_commitments_title[$i][2] : null ),
								// )
							),
						),
						'settings'			=> $tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][title]'
					)
				)
			);

			$wp_customize->add_setting(
				'tif_plugin_commitments_panel_featured' . (int)$i . '_section_sub_title_heading',
				array(
					'sanitize_callback'	=> 'tif_sanitize_key'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Heading_Control(
					$wp_customize,
					'tif_plugin_commitments_panel_featured' . (int)$i . '_section_sub_title_heading',
					array(
						'section'		=> 'tif_plugin_commitments_panel_featured' . (int)$i . '_section',
						'label'			=> esc_html__( 'Sub title', 'tif-commitments' ),
					)
				)
			);

			$tif_commitments_subtitle[$i] = tif_sanitize_wrap_attr( $featured[$i]['subtitle'] );
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][subtitle]',
				array(
					'default'			=> (array)$tif_commitments_subtitle[$i],
					'type'				=> 'option',
					'capability'		=> 'edit_theme_options',
					'sanitize_callback'	=> 'tif_sanitize_multicheck'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Wrap_Attr_Content_Control(
					$wp_customize,
					$tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][subtitle]',
					array(
						'section'			=> 'tif_plugin_commitments_panel_featured' . (int)$i . '_section',
						// 'label'				=> 'esc_html__'( 'Sub title', 'tif-commitments' ),
						'choices'           => array(
							0	=> array(
								'id'          => 'content',
								'type'        => 'text',
								// 'description' => sprintf( '%s "%s"',
								// 	esc_html__( 'Default value:', 'tif-commitments' ),
								// 	esc_html( isset( $tif_commitments_subtitle[$i][0] ) ? $tif_commitments_subtitle[$i][0] : null ),
								// )
							),
							1	=> array(
								'id'          => 'container_tag',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-commitments' ),
									esc_html( isset( $tif_commitments_subtitle[$i][1] ) ? $tif_commitments_subtitle[$i][1] : null ),
								)
							),
							2	=> array(
								'id'          => 'container_class',
								// 'description' => sprintf( '%s "%s"',
								// 	esc_html__( 'Default value:', 'tif-commitments' ),
								// 	esc_html( isset( $tif_commitments_subtitle[$i][2] ) ? $tif_commitments_subtitle[$i][2] : null ),
								// )
							),
						),
						'settings'			=> $tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][subtitle]'
					)
				)
			);

			$wp_customize->add_setting(
				'tif_plugin_commitments_panel_featured' . (int)$i . '_section_excerpt_heading',
				array(
					'sanitize_callback'	=> 'tif_sanitize_key'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Heading_Control(
					$wp_customize,
					'tif_plugin_commitments_panel_featured' . (int)$i . '_section_excerpt_heading',
					array(
						'section'		=> 'tif_plugin_commitments_panel_featured' . (int)$i . '_section',
						'label'			=> esc_html__( 'Description', 'tif-commitments' ),
					)
				)
			);

			// Add Setting
			// ...
			$tif_commitments_excerpt[$i] = tif_sanitize_wrap_attr( $featured[$i]['excerpt'] );
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][excerpt]',
				array(
					'default'			=> (array)$tif_commitments_excerpt[$i],
					'type'				=> 'option',
					'capability'		=> 'edit_theme_options',
					'sanitize_callback'	=> 'tif_sanitize_multicheck'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Wrap_Attr_Content_Control(
					$wp_customize,
					$tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][excerpt]',
					array(
						'section'			=> 'tif_plugin_commitments_panel_featured' . (int)$i . '_section',
						// 'label'				=> esc_html__( 'Description', 'tif-commitments' ),
						'choices'           => array(
							0	=> array(
								'id'          => 'content',
								'type'        => 'texarea',
								// 'description' => sprintf( '%s "%s"',
								// 	esc_html__( 'Default value:', 'tif-commitments' ),
								// 	esc_html( isset( $tif_commitments_excerpt[$i][0] ) ? $tif_commitments_excerpt[$i][0] : null ),
								// )
							),
							1	=> array(
								'id'          => 'container_tag',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-commitments' ),
									esc_html( isset( $tif_commitments_excerpt[$i][1] ) ? $tif_commitments_excerpt[$i][1] : null ),
								)
							),
							2	=> array(
								'id'          => 'container_class',
								// 'description' => sprintf( '%s "%s"',
								// 	esc_html__( 'Default value:', 'tif-commitments' ),
								// 	esc_html( isset( $tif_commitments_excerpt[$i][2] ) ? $tif_commitments_excerpt[$i][2] : null ),
								// )
							),
						),
						'settings'			=> $tif_plugin_name . '[tif_commitments_content][featured_' . $i . '][excerpt]'
					)
				)
			);

		}

	}

}

// Setup the Theme Customizer settings and controls...
add_action( 'customize_register' , array( 'Tif_Commitments_Customize' , 'register' ) );
