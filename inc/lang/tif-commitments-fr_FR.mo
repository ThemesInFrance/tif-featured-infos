��    H      \  a   �            !     0     C     L     c     q     x     �     �     �     �     �  9   �     �  
                  1     =     L  /   c     �     �     �     �     �     �                    $  1   )  
   [     f     w     �  	   �     �     �     �     �     �     �     �     	     	     '	     ,	  !   4	     V	     e	     n	     �	  !   �	     �	  
   �	  3   �	  	   �	     �	     
     -
     3
     F
     K
     Q
     Z
     j
     �
  (   �
     �
     �
  �  �
     T     f  
   }     �     �  
   �     �  
   �      �                 B   '     j     r     �     �     �     �  2   �  O     3   e  /   �     �     �     �               .     2     9  J   >  
   �     �     �  #   �  	   �     �                <  '   J     r     w     �     �     �     �  +   �     �               )  .   5  
   d     o  =   �     �  (   �  !   �               3     :     A     J      \     }  (   �     �     �                   !               H   @   +   
      *          8   6         ?       F       	       :   %                 4   )          9   1              C   3   A   5      '       E            ;      7           ,   &           >   .         "   /            B   <           =             #   0   (   G              2   -      $                    D       ... or an icon Access to settings Activate Add to generated files Allowed roles Assets Background color BlocInfo #%1$s Boxed width Button text CSS CSS enabled Congratulations, the data has been successfully recorded. Content Custom CSS Custom CSS only Customizer Enabled Description Featured Infos Featured Infos Enabled Featured Infos can be enabled in the customizer Featured Infos is disabled. Featured infos are boxed Featured title #1 Featured title #2 Featured title #3 Features Frédéric Caffin Gap Header Help Highlight important information on your homepage. Horizontal Horizontal (1/3) Horizontal (50/50) How many posts to display? In pixels Infos colors Infos layout Infos settings Layout Leave blank to disable. Link Loop layout Loop settings Need some help ? None Padding Plugin CSS (including custom css) Plugin Enabled Read now Rounded in pixels? Settings Sorry, your nonce did not verify. Spacing Text color The icon will be displayed if no image is selected. Thumbnail Thumbnail background color Tif - Featured Infos Title Unauthorized user. Unit Value Vertical Width in pixels You can choose an image... https://themesinfrance.fr https://themesinfrance.fr/plugins/infos/ px rem Project-Id-Version: Tif - Featured Infos 0.1
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/tif-featured-infos
PO-Revision-Date: 2021-10-18 10:44+0200
Last-Translator: Frédéric Caffin <fc@xymail.fr>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
X-Domain: tif-featured-infos
 ... ou une icône Accès aux paramètres Activation Ajouter aux fichiers générés Autorisations Ressources Couleur d’arrière-plan Info #%1$s Largeur des éléments encadrés Texte du bouton CSS CSS activé Félicitations, les données ont été enregistrées avec succès. Contenu CSS personnalisé CSS personnalisé seulement Personnalisation activée Description Informations mises en avant Les informations mises en avant sont désactivées Les informations mises en avant peuvent être activée dans la personnalisation Les informations mises en avant sont désactivées. Les informations mises en avant sont encadrées Titre mis en avant #1 Titre mis en avant #3 Titre mis en avant #3 Fonctionnalités Frédéric Caffin Gap Header Aide Mettez en évidence les informations importantes sur votre page d'accueil. Horizontal Horizontal (1/3) Horizontal (50/50) Combien d’informations afficher ? En pixels Couleurs des informations Mise en forme des informations Paramètres des informations Mise en forme Laissez le champ vide pour désactiver. Lien Mise en forme de la boucle Paramètres de la boucle Besoin d'aide ? Aucun Remplissage CSS du plugin (inclut le css personnalisé) Plugin activé Lire la suite Arrondi en pixel ? Paramètres Désolé, votre nonce n'a pas été vérifié. Espacement Couleur du texte L'icône sera affichée si aucune image n'est sélectionnée. Vignette Couleur d’arrière-plan de la vignette Tif - Informations mises en avant Titre Utilisateur non autorisé. Unité Valeur Vertical Largeur en pixels Vous pouvez choisir une image... https://themesinfrance.fr https://themesinfrance.fr/plugins/infos/ px rem 